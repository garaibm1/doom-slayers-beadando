using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour
{
    public static bool gameIsWon = false;

    public void OnTriggerEnter()
    {
        SceneManager.LoadScene("Win");
        gameIsWon = true;
    }
}