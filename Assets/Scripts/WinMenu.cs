using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour
{
    public void ReplayGame()
    {
        SceneManager.LoadScene("Game");
        EndTrigger.gameIsWon = false;
    }
}