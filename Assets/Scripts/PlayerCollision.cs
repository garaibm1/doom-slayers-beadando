using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollision : MonoBehaviour
{
    public bool hit = false;
    public static bool gameIsLost = false;

    private void Update()
    {
        if (hit)
        {
            Debug.Log("Hit!");
            hit = false;
        }
    }

    public void OnTriggerEnter(Collider Enemy)
    {
        hit = true;
        //SceneManager.LoadScene("Lose");
        gameIsLost = true;
    }
}