Puzsér Run

Garai Bálint - garaibm1@gmail.com
Pontyos István - ponista12@gmail.com
Vass Máté - matevass01@gmail.com

Puzsér Róbert nagyon mérges lett játékunk főhősére, mert cringe-et posztolt,
ezért elhatározta, hogy megleckézteti.
Meneküléséhez főhősünknek át kell szelnie a Pusztító Alpáriság Labirintusát
és a Szellemi Kútmérgezés Ingoványait. Nincs azonban könnyű dolga, ugyanis
közben az elszánt publicista káromkodások közepette igyekszik elkapni.

Tutorial:
WASD - mozgás
SPACE - ugrás
QER - ???

A karakter mozgását Input Systemmel oldottuk meg, az Enemy mozgását pedig a NavMesh komponenssel.
